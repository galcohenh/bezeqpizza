import { AfterContentInit, ElementRef, EventEmitter, Input, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { PizzaMenuItemDTO } from 'src/app/common/entities/Dtos/add-pizza-dto.type';
import { PizzasService } from 'src/app/common/services/pizza.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'create-order-page',
  templateUrl: './create-order-page.component.html',
  styleUrls: ['./create-order-page.component.scss']
})
export class CreateOrderPageComponent implements OnInit {

  displayedColumns: string[] = ['name'];

  orderForm: FormGroup;
  pizzaItems: PizzaMenuItemDTO[];

  orderDataSource = new MatTableDataSource();

  constructor(private formBuilder: FormBuilder, private readonly pizzaService: PizzasService) {
  }

  ngOnInit(): void {
    this.orderForm = this.formBuilder.group({
      order: '',
    });

    this.pizzaService.getAllPizzas().subscribe((pizzas) => {
      this.pizzaItems = pizzas;
    });

    this.pizzaService.getUserOrders().subscribe((orders) => {
      this.orderDataSource.data = orders;
    });
  }

  public addOrder(): void {
    const order = this.orderForm.get("order").value;
    this.pizzaService.addOrder(order).subscribe((response) => {
      this.orderDataSource.data = [...this.orderDataSource.data, order];
      Swal.fire({
        title: 'New menu item was added!',
        icon: 'success',
        color: "white",
        background: "#1D1E28",
        confirmButtonText: "Submit",
        confirmButtonColor: "#7066e0",
        heightAuto: false,
      });
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.orderDataSource.filter = filterValue.trim().toLowerCase();
  }
}
