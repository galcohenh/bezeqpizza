import { AfterContentInit, ElementRef, EventEmitter, Input, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { PizzaMenuItemDTO } from 'src/app/common/entities/Dtos/add-pizza-dto.type';
import { PizzasService } from 'src/app/common/services/pizza.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'manage-orders-page',
  templateUrl: './manage-pizza-page.component.html',
  styleUrls: ['./manage-pizza-page.component.scss']
})
export class ManagePizzasPageComponent implements OnInit {

  displayedColumns: string[] = ['name', 'description', 'price'];

  pizzaForm: FormGroup;

  pizzaDataSource = new MatTableDataSource();

  constructor(private formBuilder: FormBuilder, private readonly pizzaService: PizzasService) { 
    this.createForm();
  }

  ngOnInit(): void {
    this.pizzaForm = this.formBuilder.group({
      name: '',
      description: '',
      price: ''
    });

    this.pizzaService.getAllPizzas().subscribe((pizzas) => {
      this.pizzaDataSource.data = pizzas;
    });
  }

  createForm() {
    this.pizzaForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      price: ['', Validators.required]
    });
  }

  public addPizza(): void {
    const name = this.pizzaForm.get("name").value;
    const description = this.pizzaForm.get("description").value;
    const price = this.pizzaForm.get("price").value;
    const pizzaMenuItemDTO = new PizzaMenuItemDTO(name, description, price);
    this.pizzaService.addPizza(pizzaMenuItemDTO).subscribe((response) => {
      this.pizzaDataSource.data = [...this.pizzaDataSource.data, pizzaMenuItemDTO];
      Swal.fire({
        title: 'New menu item was added!',
        icon: 'success',
        color: "white",
        background: "#1D1E28",
        confirmButtonText: "Submit",
        confirmButtonColor: "#7066e0",
        heightAuto: false,
      });
      
    }, (error) => {})
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.pizzaDataSource.filter = filterValue.trim().toLowerCase();
  }
}
