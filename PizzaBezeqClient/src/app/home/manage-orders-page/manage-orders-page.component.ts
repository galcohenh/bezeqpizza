import { AfterContentInit, ElementRef, EventEmitter, Input, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material';
import { PizzaMenuItemDTO } from 'src/app/common/entities/Dtos/add-pizza-dto.type';
import { PizzasService } from 'src/app/common/services/pizza.service';

@Component({
  selector: 'manage-orders-page',
  templateUrl: './manage-orders-page.component.html',
  styleUrls: ['./manage-orders-page.component.scss']
})
export class ManageOrdersPageComponent implements OnInit {

  displayedColumns: string[] = ['name', 'order', 'state' ];

  orderForm: FormGroup;

  orderDataSource = new MatTableDataSource();

  constructor(private formBuilder: FormBuilder, private readonly pizzaService: PizzasService) { 
    this.createForm();
  }

  ngOnInit(): void {
    this.orderForm = this.formBuilder.group({
      name: '',
      order: '',
    });
    this.pizzaService.getAdminOrders().subscribe(orders => {
      this.orderDataSource.data = orders;
    });
  }

  createForm() {
    this.orderForm = this.formBuilder.group({
      name: ['', Validators.required],
      order: ['', Validators.required],
    });
  }

  public addOrder(): void {
    const name = this.orderForm.get("name").value;
    const order = this.orderForm.get("order").value;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.orderDataSource.filter = filterValue.trim().toLowerCase();
  }
}
