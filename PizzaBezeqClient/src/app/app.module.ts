
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from '../app/common/login-page/login-page.component';
import { IconsModule } from './icons.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HomePageComponent } from './home/home-page/home-page.component';
import { SideBarComponent } from './common/side-bar/side-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutPageComponent } from './home/about/about.component';
import { RegisterPanelComponent } from './common/login-page/register-panel/register-panel.component';
import { LoginPanelComponent } from './common/login-page/login-panel/login-panel.component';
import { MatTabsModule, MatCardModule, MatStepperModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatChipsModule, MatAutocomplete, MatAutocompleteModule, MatIconModule, MatSliderModule, MatDatepickerModule, MatRadioModule, MatNativeDateModule, MatTableModule, MatSelectModule } from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { ManagePizzasPageComponent } from './home/manage-pizza-page/manage-pizza-page.component';
import { ManageOrdersPageComponent } from './home/manage-orders-page/manage-orders-page.component';
import { CreateOrderPageComponent } from './home/create-order-page/create-order-page.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HomePageComponent,
    SideBarComponent,
    RegisterPanelComponent,
    AboutPageComponent,
    LoginPanelComponent,
    ManagePizzasPageComponent,
    ManageOrdersPageComponent,
    CreateOrderPageComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IconsModule,
    FormsModule,
    HttpClientModule,
    MatTabsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatStepperModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatChipsModule,
    MatAutocompleteModule, 
    MatFormFieldModule,
    MatIconModule,
    NgxChartsModule,
    MatSliderModule,
    MatFormFieldModule,
    NgxSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTableModule,
    MatSelectModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
