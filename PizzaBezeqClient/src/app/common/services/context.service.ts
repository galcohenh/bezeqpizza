import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContextService {

  public USERS_MANAGER_URL = "http://localhost:5000/users"; 
  public PIZZA_MANAGER_URL = " http://localhost:5000/pizza";
  public ORDER_MANAGER_URL = " http://localhost:5000/order";

  constructor() { }
}
