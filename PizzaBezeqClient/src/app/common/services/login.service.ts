import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from "@angular/router";
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ContextService } from './context.service';
import { CookiesService } from './cookies.service';
import { LoginRequestDTO } from '../entities/Dtos/login-request-dto.type';
import { RegisterRequestDTO } from '../entities/Dtos/register-request-dto';

@Injectable({
  providedIn: 'root'
})
export class LoginService implements OnInit {

  constructor(private readonly http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private contextService: ContextService,
    private cookiesService: CookiesService) { }

  ngOnInit(): void {}

  public tryLogin(): void {
    const token = this.cookiesService.getAuthToken();
    if (token) {
      this.router.navigate(['/']);
    }
  }

  public isServerAlive(): Observable<boolean> {
    return this.http.get<boolean>(`${this.contextService.USERS_MANAGER_URL}/isAlive`);
  }

    public isAdmin(): Observable<boolean> {
      const token = this.cookiesService.getAuthToken();
    return this.http.get<boolean>(`${this.contextService.USERS_MANAGER_URL}/isAdmin/${token}`);
  }

  public logIn(loginRequest: LoginRequestDTO): Observable<boolean> {
    var subject = new Subject<boolean>();
    this.http.post<{authToken: string}>(`${this.contextService.USERS_MANAGER_URL}/login`, loginRequest).subscribe((response) => {
      this.cookiesService.setAuthToken(response.authToken);
      console.log("email", loginRequest)
      localStorage.setItem('userName', (<any>loginRequest).email);
      this.router.navigate(['/'], { relativeTo: this.route });
      subject.next(true);
    }, (error) => {
      subject.next(false);
    })
    return subject.asObservable();
  }

  public logOut(): void {
    this.cookiesService.clearCookies();
    localStorage.removeItem('userName');
    this.router.navigate(['/login']);
  }

  public register(registerRequest: RegisterRequestDTO): Observable<boolean> {
    var subject = new Subject<boolean>();
    this.http.post<{authToken: string}>(`${this.contextService.USERS_MANAGER_URL}/register`, registerRequest).subscribe((response) => {
      this.cookiesService.setAuthToken(response.authToken)
      localStorage.setItem('userName', registerRequest.Email);
      this.router.navigate(['/home/selectType'], { relativeTo: this.route });
      subject.next(true);
    }, (error) => {
      subject.next(false);
    })
    return subject.asObservable();
  }
}
