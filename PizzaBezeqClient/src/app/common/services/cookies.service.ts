import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CookiesService {
  private readonly AUTH_TOKEN_COOKIE = "auth_token";

  constructor() { }

  public setAuthToken(authToken: string): void {
    document.cookie = `${this.AUTH_TOKEN_COOKIE}=${authToken}`;
  }

  public getAuthToken(): string {
    return this.getCookie(this.AUTH_TOKEN_COOKIE);
  }

  private getCookie(cname: string) {
    const name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        const cookieValue = c.substring(name.length, c.length);
        if (cookieValue) {
          return cookieValue;
        }
      }
      
    }
    return "";
  }

  public clearCookies() {
    document.cookie = `${this.AUTH_TOKEN_COOKIE}=`;
  }
}
