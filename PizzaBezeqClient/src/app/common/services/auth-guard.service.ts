import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CookiesService } from './cookies.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private readonly router: Router, private cookiesService: CookiesService) {}

  canActivate(): boolean {
    const token = this.cookiesService.getAuthToken();
    if (!token) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}