import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppStateService {
  private isSideBarOpenEmitter: EventEmitter<boolean>;
  private isSideBarOpen: boolean;

  constructor() { 
    this.isSideBarOpenEmitter = new EventEmitter<boolean>();
  }

  public getSideBarStatus(): boolean {
    return this.isSideBarOpen;
  }

  public setSideBarStatus(value?: boolean) {
      if (value) {
        if (this.isSideBarOpen != value) {
          this.isSideBarOpen = value;
          this.isSideBarOpenEmitter.emit(value);
        }
      } else {
        this.isSideBarOpen = !this.isSideBarOpen;
        this.isSideBarOpenEmitter.emit(this.isSideBarOpen);
      }
  }
}
