import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ContextService } from './context.service';
import { PizzaMenuItemDTO } from '../entities/Dtos/add-pizza-dto.type';
import { AddOrderDto } from '../entities/Dtos/add-order-dto.type';
import { CookiesService } from './cookies.service';
import { OrderDto } from '../entities/Dtos/order-dto.type';

@Injectable({
  providedIn: 'root'
})
export class PizzasService {

  constructor(private readonly http: HttpClient,
    private readonly contextService: ContextService, 
    private cookiesService: CookiesService) { }

    public addPizza(addPizzaRequest: PizzaMenuItemDTO): Observable<any> {
      return this.http.post(`${this.contextService.PIZZA_MANAGER_URL}`, addPizzaRequest);
    }

    public addOrder(order: string): Observable<any> {
      let addOrderRequest = new AddOrderDto(this.cookiesService.getAuthToken(), order);
      return this.http.post(`${this.contextService.ORDER_MANAGER_URL}/OrderPizza`, addOrderRequest);
    }

    public getUserOrders(): Observable<string[]> {
      return this.http.get<string[]>(`${this.contextService.ORDER_MANAGER_URL}/${this.cookiesService.getAuthToken()}`);
    }

    public getAdminOrders(): Observable<OrderDto[]> {
      return this.http.get<OrderDto[]>(`${this.contextService.ORDER_MANAGER_URL}/GetAdminOrders/${this.cookiesService.getAuthToken()}`);
    }
    
    public getAllPizzas(): Observable<PizzaMenuItemDTO[]> {
      return this.http.get<PizzaMenuItemDTO[]>(`${this.contextService.PIZZA_MANAGER_URL}`);
    }
}
