import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';
import { AppStateService } from '../services/app-state.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit, AfterViewInit {

  isAdmin: boolean;

  constructor(private _appState: AppStateService, private router: Router, private loginService: LoginService) {
  }
  ngAfterViewInit(): void {
    this.loginService.isAdmin().subscribe((isAdmin) => {
      this.isAdmin = isAdmin;
    });
  }
  
  ngOnInit() {}
  
  toggleSideBar() {
    this._appState.setSideBarStatus();
  }

  logout() {
    this.loginService.logOut();
  }
}