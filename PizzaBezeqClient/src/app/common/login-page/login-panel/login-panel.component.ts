import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginRequestDTO } from '../../entities/Dtos/login-request-dto.type';

@Component({
  selector: 'login-panel',
  templateUrl: './login-panel.component.html',
  styleUrls: ['./login-panel.component.scss']
})
export class LoginPanelComponent implements OnInit {

  private username: string;
  private password: string;
  private loginRequest: LoginRequestDTO;
  private usernameWarning: boolean;
  private logInWarning: boolean;
  private serverDisconnected: boolean;

  constructor(private readonly loginService: LoginService) { }

  ngOnInit() {
    this.usernameWarning = false;
    this.logInWarning = false;
    this.serverDisconnected = false;
    this.loginRequest = new LoginRequestDTO();
  }

  async signIn() {
    this.serverDisconnected = true;
    this.loginService.isServerAlive().subscribe(()=> {
      this.serverDisconnected = false;
      this.logInWarning = false;
      this.loginService.logIn(this.loginRequest).subscribe((isLoginSucceed) => {
        if(!isLoginSucceed) {
          this.logInWarning = true;
        }
      })
    })
  }
}
