import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  
  constructor(private readonly loginService: LoginService) {}
    
  private displayLogin: boolean;

  ngOnInit() {
    this.loginService.tryLogin();
    this.displayLogin = true;
  }
}
