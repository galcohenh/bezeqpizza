import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { RegisterRequestDTO } from '../../entities/Dtos/register-request-dto';
import { LoginService } from '../../services/login.service';
import { GenderType } from '../../enums/gender-type.enum';

@Component({
  selector: 'register-panel',
  templateUrl: './register-panel.component.html',
  styleUrls: ['./register-panel.component.scss']
})
export class RegisterPanelComponent implements OnInit {
  private checkPasswords: ValidatorFn = (group: AbstractControl):  ValidationErrors | null => { 
    let pass = group.get('passwordCtrl').value;
    let confirmPass = group.get('confirmPasswordCtrl').value
    return pass === confirmPass ? null : { notSame: true }
  }

  nameFormGroup: FormGroup = this._formBuilder.group({fullNameCtrl: ['', [Validators.required]]});
  emailFormGroup: FormGroup = this._formBuilder.group({emailCtrl: ['', [Validators.required, Validators.email, Validators.min(5)]]});
  passwordFormGroup: FormGroup = this._formBuilder.group({passwordCtrl: ['', [Validators.required, Validators.min(5)]], confirmPasswordCtrl: ['']}, { validators: this.checkPasswords });
  dobFormGroup: FormGroup = this._formBuilder.group({dobCtrl: ['', [Validators.required]]});
  genderFormGroup: FormGroup = this._formBuilder.group({genderCtrl: [GenderType.Female, [Validators.required]]});
  private isPasswordShown: boolean = false;

  constructor(private readonly _formBuilder: FormBuilder, private readonly loginService: LoginService) { }

  ngOnInit() {
  }

  public register(): void {
    const fullName = this.nameFormGroup.get("fullNameCtrl").value;
    const email = this.emailFormGroup.get("emailCtrl").value;
    const password = this.passwordFormGroup.get("passwordCtrl").value;
    const date = this.dobFormGroup.get("dobCtrl").value;
    const gender = this.genderFormGroup.get("genderCtrl").value;
    const registerRequest = new RegisterRequestDTO(fullName, email, password, date, gender);
    this.loginService.register(registerRequest);
  }
}
