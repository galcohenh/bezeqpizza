export class AddOrderDto {
    authToken: string;
    orderId: string;

    constructor(authToken: string, orderId: string) {
        this.authToken = authToken;
        this.orderId = orderId;
    }
}