export class RegisterRequestDTO {
    FullName: string;
    Email: string;
    Password: string;
    DateOfBirth: Date;
    Gender: string;
    constructor(fullName?: string, email?: string, password?: string, dateOfBirth?: Date, gender?: string) {
        this.FullName = fullName;
        this.Email = email;
        this.Password = password;
        this.DateOfBirth = dateOfBirth;
        this.Gender = gender;
    }
}