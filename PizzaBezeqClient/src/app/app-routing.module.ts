import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from '../app/common/login-page/login-page.component';
import { HomePageComponent } from './home/home-page/home-page.component';
import { AuthGuard } from './common/services/auth-guard.service';
import { AboutPageComponent } from './home/about/about.component';
import { ManagePizzasPageComponent } from './home/manage-pizza-page/manage-pizza-page.component';
import { ManageOrdersPageComponent } from './home/manage-orders-page/manage-orders-page.component';
import { CreateOrderPageComponent } from './home/create-order-page/create-order-page.component';


const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent },
  {
    path: 'home', component: HomePageComponent, canActivate: [AuthGuard],
    children: [
      { path: 'manageMenuItems', component: ManagePizzasPageComponent },
      { path: 'manageOrders', component: ManageOrdersPageComponent },
      { path: 'createOrder', component: CreateOrderPageComponent },
      { path: 'about', component: AboutPageComponent },
      { path: '**', component: ManagePizzasPageComponent }
    ]
  },
  { path: '**', component: LoginPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
