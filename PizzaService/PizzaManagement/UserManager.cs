﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PizzaManagement
{
    public class UserManager
    {
        private static UserManager _instance;
        private readonly MongoManager _mongoManager;
        private readonly IMongoCollection<UserModel> _usersCollection;
        private const string DB_NAME = "BezeqPizza";
        private const string COLLECTION_NAME = "Users";

        public static UserManager GetInstance()
        {
            return _instance ??= new UserManager();
        }

        public UserManager()
        {
            _mongoManager = new MongoManager(DB_NAME);
            _usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
        }

        public async Task<string> TryLogin(string email, string password)
        {
            // Look up the user in the database
            var filter = Builders<UserModel>.Filter.Eq(user => user.Email, email);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null)
            {
                return String.Empty;
            }

            // Check the password
            string hashedPassword = user.Password;
            if (!VerifyPassword(password, hashedPassword))
            {
                return String.Empty;
            }

            // Generate a new authentication token
            string authToken = Guid.NewGuid().ToString();

            // Update the user's record to include the new auth token
            var update = Builders<UserModel>.Update.Set(user => user.AuthToken, authToken);
            await _usersCollection.UpdateOneAsync(filter, update);

            return authToken;
        }

        public async Task<string> TryRegister(string fullName, string email, string password, DateTime dateOfBirth, string gender)
        {
            // Look up the user in the database
            var filter = Builders<UserModel>.Filter.Eq(user => user.Email, email);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user != null)
            {
                return String.Empty;
            }

            string hashedPassword = HashPassword(password);
            // Generate a new authentication token
            string authToken = Guid.NewGuid().ToString();
            var userDocument = new UserModel()
            {
                Email = email,
                Password = hashedPassword,
                FullName = fullName,
                AuthToken = authToken,
                DateOfBirth = dateOfBirth,
                Gender = gender,
                Orders = new List<string>()
            };
            await _usersCollection.InsertOneAsync(userDocument);
            return authToken;
        }

        private static string HashPassword(string password)
        {
            // Convert the password and hashed password to byte arrays
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            // Create a new instance of the hash algorithm
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();


            // Calculate the hash of the provided password
            byte[] passwordHash = hashAlgorithm.ComputeHash(passwordBytes);

            return Convert.ToBase64String(passwordHash);
        }

        public async Task<bool> ValidateAuthToken(string authToken)
        {
            // Look up the user in the database
            var usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await usersCollection.Find(filter).SingleOrDefaultAsync();
            return user != null;
        }

        private bool VerifyPassword(string password, string hashedPassword)
        {
            // Convert the password and hashed password to byte arrays
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] hashedPasswordBytes = Convert.FromBase64String(hashedPassword);

            // Create a new instance of the hash algorithm
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();


            // Calculate the hash of the provided password
            byte[] passwordHash = hashAlgorithm.ComputeHash(passwordBytes);

            // Compare the hashed password to the provided password hash
            return hashedPasswordBytes.SequenceEqual(passwordHash);
        }

        public async Task<string> GetMailByAuth(string authToken)
        {
            // Look up the user in the database
            var usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null)
            {
                return string.Empty;
            }
            return user.Email;
        }

        public async Task UpdateOrdersList(string authToken, string order)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var update = Builders<UserModel>.Update.AddToSet(user => user.Orders, order);
            await _usersCollection.UpdateOneAsync(filter, update);
        }

        public async Task<List<string>> GetUserOrdersList(string authToken)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            return user.Orders;
        }

        public async Task<IEnumerable<(string, string)>> GetAdminOrdersList(string authToken)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            return (await _usersCollection.Find(_ => true).ToListAsync()).Select(user => (user.FullName, string.Join(", ", user.Orders)));
        }
        public async Task<ObjectId> GetIdByAuth(string authToken)
        {
            // Look up the user in the database
            var usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(COLLECTION_NAME);
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null)
            {
                return ObjectId.Empty;
            }
            return user.Id;
        }

        public async Task<bool> IsAdmin(string authToken)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            return user.IsAdmin;
        }
    }
}
