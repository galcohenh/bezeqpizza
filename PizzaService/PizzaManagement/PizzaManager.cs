﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PizzaManagement
{
    public class PizzaManager
    {
        private static PizzaManager _instance;
        private readonly MongoManager _mongoManager;
        private readonly IMongoCollection<PizzaModel> _pizzasCollection;
        private const string DB_NAME = "BezeqPizza";
        private const string COLLECTION_NAME = "Pizzas";

        public static PizzaManager GetInstance()
        {
            return _instance ??= new PizzaManager();
        }

        public PizzaManager()
        {
            _mongoManager = new MongoManager(DB_NAME);
            _pizzasCollection = _mongoManager.MongoDatabase.GetCollection<PizzaModel>(COLLECTION_NAME);
        }

        public async Task AddPizza(string name, string description, int price)
        {
            var pizzaDocument = new PizzaModel()
            {
                name = name,
                description = description,
                price = price
            };
            await _pizzasCollection.InsertOneAsync(pizzaDocument);
        }

        public async Task<List<PizzaModel>> GetAllPizzas()
        {
            return await _pizzasCollection.Find(_ => true).ToListAsync();
        }
    }
}
