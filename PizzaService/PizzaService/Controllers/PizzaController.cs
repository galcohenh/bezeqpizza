using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using PizzaManagement;
using PizzaService.Model;
using Microsoft.AspNetCore.Cors;

namespace PizzaService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class PizzaController : Controller
    {
        private readonly ILogger<PizzaController> _logger;
        private readonly PizzaManager _pizzaManager;
        private readonly UserManager _userManager;

        public PizzaController(ILogger<PizzaController> logger, UserManager userManager, PizzaManager pizzaManager)
        {
            _logger = logger;
            _userManager = userManager;
            _pizzaManager = pizzaManager;
        }

        [HttpPost]
        public async Task<IActionResult> UploadPizza([FromBody] PizzaMenuItemDTO addPizzaRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _pizzaManager.AddPizza(addPizzaRequest.Name, addPizzaRequest.Description, addPizzaRequest.Price);
            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult<PizzaMenuItemDTO>> GetAllPizzas()
        {
            // Validate the request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Return the auth token to the client
            return Ok(await _pizzaManager.GetAllPizzas());
        }
    }
}
