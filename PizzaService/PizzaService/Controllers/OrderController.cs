﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PizzaManagement;
using PizzaService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class OrderController : Controller
    {
        private readonly ILogger<OrderController> _logger;
        private readonly PizzaManager _pizzaManager;
        private readonly UserManager _userManager;

        public OrderController(ILogger<OrderController> logger, UserManager userManager, PizzaManager pizzaManager)
        {
            _logger = logger;
            _userManager = userManager;
            _pizzaManager = pizzaManager;
        }

        [HttpPost("OrderPizza")]
        public async Task<IActionResult> OrderPizza([FromBody] UserOrderDTO addPizzaRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _userManager.UpdateOrdersList(addPizzaRequest.AuthToken, addPizzaRequest.OrderId);
            return Ok();
        }

        [HttpGet("{authToken}")]
        public async Task<ActionResult<IEnumerable<string>>> GetUserOrders(string authToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            return Ok(await _userManager.GetUserOrdersList(authToken));
        }

        [HttpGet("GetAdminOrders/{authToken}")]
        public async Task<ActionResult<IEnumerable<AdminOrderDTO>>> GetAdminOrders(string authToken)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (String.IsNullOrEmpty(authToken) || !(await _userManager.IsAdmin(authToken)))
            {
                return Unauthorized();
            }

            var adminOrders = await _userManager.GetAdminOrdersList(authToken);
            var list = new List<AdminOrderDTO>();
            foreach (var adminOrder in adminOrders)
            {
                list.Add(new AdminOrderDTO()
                {
                    Name = adminOrder.Item1,
                    Order = adminOrder.Item2
                });
            }
            return Ok(list);
        }
    }
}
