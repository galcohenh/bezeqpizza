﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using PizzaManagement;
using PizzaService.Model;

namespace PizzaService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private readonly UserManager _userManager;

        public UsersController(ILogger<UsersController> logger, UserManager userManager)
        {
            _logger = logger;
            _userManager = userManager;
        }

        /// <summary>
        /// checks if the server side alive
        /// </summary>
        /// <returns>is server side alive</returns>
        [Route("isAlive")]
        [HttpGet]
        public bool IsAlive()
        {
            return true;
        }

        [Route("login")]
        [HttpPost]
        public async Task<ActionResult<string>> Login([FromBody] LoginRequestDTO loginRequest)
        {
            // Validate the request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string authToken = await _userManager.TryLogin(loginRequest.Email, loginRequest.Password);
            if (String.IsNullOrEmpty(authToken))
            {
                return Unauthorized();
            }
            // Return the auth token to the client
            return Ok(new { authToken });
        }

        [Route("isAdmin/{authToken}")]
        [HttpGet]
        public async Task<ActionResult<string>> IsAdmin(string authToken)
        {
            return Ok(await _userManager.IsAdmin(authToken));
        }

        [Route("register")]
        [HttpPost]
        public async Task<ActionResult<string>> Register([FromBody] RegisterRequestDTO registerRequest)
        {
            // Validate the request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string authToken = await _userManager.TryRegister(registerRequest.FullName, registerRequest.Email, registerRequest.Password, registerRequest.DateOfBirth, registerRequest.Gender);
            if (String.IsNullOrEmpty(authToken))
            {
                return Unauthorized();
            }
            // Return the auth token to the client
            return Ok(new { authToken });
        }
    }
}