﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaService.Model
{
    public class UserOrderDTO
    {
        public string AuthToken { get; set; }
        public string OrderId { get; set; }
    }
}
