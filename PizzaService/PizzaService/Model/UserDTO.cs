﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaService.Model
{
    public class UserDTO
    {
        public string Username { get; set; }
        public string MailAddress { get; set; }
        public string Gender { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
