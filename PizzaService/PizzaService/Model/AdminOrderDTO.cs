﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaService.Model
{
    public class AdminOrderDTO
    {
        public string Name { get; set; }
        public string Order { get; set; }
    }
}
